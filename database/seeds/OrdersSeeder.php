<?php

use Illuminate\Database\Seeder;
use App\Entity\Buyer;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Buyer::class, 10)->create()
            ->each(function (Buyer $buyer) {

                $buyer->orders()->saveMany(
                    factory(Order::class, 5)->create(['buyer_id' => $buyer->id()])
                        ->each(function (Order $order) {

                            $order->orderItems()->saveMany(
                                factory(OrderItem::class, 5)->make([
                                    'order_id' => $order->id(),
                                    'product_id' => Product::all()->random()->first()->id
                                ])
                            );

                        })
                );

            });
    }
}
