<?php

use App\Entity\Product;
use App\Entity\Seller;
use Illuminate\Database\Seeder;

class SellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Seller::class, 10)->create()
            ->each(function (Seller $seller){
                $seller->products()->saveMany(
                    factory(Product::class, 20)->make(['seller_id' => null])
                );
            });
    }
}
