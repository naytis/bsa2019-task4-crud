<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entity\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'quantity' => $faker->numberBetween(1, 100),
        'price' => $faker->numberBetween(100, 100000),
        'discount' => $faker->numberBetween(0, 100)
    ];
});
