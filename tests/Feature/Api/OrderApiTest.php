<?php

namespace Tests\Feature\Api;

use App\Entity\Order;
use App\Exceptions\ErrorCode;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\CreatesApplication;

class OrderApiTest extends BaseTestCase
{
    use CreatesApplication;

    protected const ROOT_RESPONSE_KEY = 'data';

    protected const ORDER_RESOURCE_STRUCTURE = [
        'orderId',
        'orderDate',
        'orderSum',
        'orderItems' => ['*' => self::ORDER_ITEM_STRUCTURE],
        'buyer' => self::BUYER_RESOURCE_STRUCTURE
    ];

    protected const ORDER_ITEM_STRUCTURE = [
        'productName',
        'productQty',
        'productPrice',
        'productDiscount',
        'productSum'
    ];

    protected const BUYER_RESOURCE_STRUCTURE = [
        'buyerFullName',
        'buyerAddress',
        'buyerPhone'
    ];

    private const API_URL = 'api/orders';

    public function test_get_all_orders()
    {
        $response = $this->get(self::API_URL)->assertOk();

        $this->assertNotEmpty($response->json('data'));

        $response->assertJsonStructure([
            self::ROOT_RESPONSE_KEY => ['*' => self::ORDER_RESOURCE_STRUCTURE]
        ]);
    }

    public function test_get_order_by_id()
    {
        $order = Order::first();

        $uri = $this->createResourceItemUri(self::API_URL, $order->id);

        $this->get($uri)
            ->assertOk()
            ->assertJsonStructure([
                self::ROOT_RESPONSE_KEY => self::ORDER_RESOURCE_STRUCTURE
            ]);
    }

    public function test_get_order_by_id_not_found()
    {
        $uri = $this->createResourceItemUri(self::API_URL, 999);

        $this->get($uri)
            ->assertNotFound()
            ->assertExactJson([
                'errors' => [
                    [
                        'code' => ErrorCode::NOT_FOUND,
                        'message' => 'Order not found.'
                    ]
                ]
            ]);
    }

    public function test_add_order()
    {
        $attributes = [
            "buyerId" => 1,
            "orderItems" => [
                [
                    "productId" => 1,
                    "productQty" => 1,
                    "productDiscount" => 1
                ]
            ]
        ];

        $this->json('POST', self::API_URL, $attributes)
            ->assertStatus(201)
            ->assertJsonStructure([
                self::ROOT_RESPONSE_KEY => self::ORDER_RESOURCE_STRUCTURE
            ]);
    }

    public function test_add_order_buyer_not_found()
    {
        $attributes = [
            "buyerId" => 999,
            "orderItems" => [
                [
                    "productId" => 1,
                    "productQty" => 1,
                    "productDiscount" => 1
                ]
            ]
        ];

        $this->json('POST', self::API_URL, $attributes)
            ->assertNotFound()
            ->assertExactJson([
                'errors' => [
                    [
                        'code' => ErrorCode::NOT_FOUND,
                        'message' => 'Buyer not found.'
                    ]
                ]
            ]);
    }

    public function test_add_order_product_not_found()
    {
        $attributes = [
            "buyerId" => 1,
            "orderItems" => [
                [
                    "productId" => 999,
                    "productQty" => 1,
                    "productDiscount" => 1
                ]
            ]
        ];

        $this->json('POST', self::API_URL, $attributes)
            ->assertNotFound()
            ->assertExactJson([
                'errors' => [
                    [
                        'code' => ErrorCode::NOT_FOUND,
                        'message' => 'Product not found.'
                    ]
                ]
            ]);
    }

    public function test_add_order_invalid_request_params()
    {
        $attributes = [
            "buyerId" => 1
        ];

        $this->json('POST', self::API_URL, $attributes)
            ->assertStatus(400)
            ->assertJsonStructure(['errors' => []]);
    }

    public function test_update_order_by_id()
    {
        $order = Order::first();
        $uri = $this->createResourceItemUri(self::API_URL, $order->id);
        $attributes = [
            "orderItems" => [
                [
                    "productId" => 1,
                    "productQty" => 1,
                    "productDiscount" => 1
                ]
            ]
        ];

        $this->json('PUT', $uri, $attributes)->assertStatus(200);
    }

    public function test_delete_order_by_id()
    {
        $order = Order::first();
        $uri = $this->createResourceItemUri(self::API_URL, $order->id);
        $this->delete($uri)->assertStatus(204);
    }

    public function test_delete_order_by_id_not_found()
    {
        $uri = $this->createResourceItemUri(self::API_URL, 999);

        $this->delete($uri)
            ->assertNotFound()
            ->assertExactJson([
                'errors' => [
                    [
                        'code' => ErrorCode::NOT_FOUND,
                        'message' => 'Order not found.'
                    ]
                ]
            ]);
    }

    private function createResourceItemUri(string $uri, int $id): string
    {
        return $uri . '/' . $id;
    }
}
