<?php

declare(strict_types=1);

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

final class Seller extends Model
{
    public function products()
    {
        return $this->hasMany(Product::class, 'seller_id', 'id');
    }
}
