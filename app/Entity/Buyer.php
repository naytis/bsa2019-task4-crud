<?php

declare(strict_types=1);

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Buyer
 * @package App\Entity
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $country
 * @property string $city
 * @property string $address_line
 * @property string $phone
 * @property Carbon $created_ad
 * @property Carbon|null $updated_at
 */
final class Buyer extends Model
{
    protected $table = 'buyers';

    protected $fillable = [
        'name',
        'surname',
        'country',
        'city',
        'address_line',
        'phone'
    ];

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function surname(): string
    {
        return $this->surname;
    }

    public function country(): string
    {
        return $this->country;
    }

    public function city(): string
    {
        return $this->city;
    }

    public function addressLine(): string
    {
        return $this->address_line;
    }

    public function phone(): string
    {
        return $this->phone;
    }
}
