<?php

declare(strict_types=1);

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Product
 * @package App\Entity
 * @property int $id
 * @property string $name
 * @property int $price
 * @property bool $available
 * @property int $seller_id
 */
final class Product extends Model
{
    protected $table = 'products';

    protected $attributes = [
        'available' => false,
    ];

    protected $fillable = [
        'name',
        'price',
        'available',
        'seller_id'
    ];

    public function seller(): BelongsTo
    {
        return $this->belongsTo(Seller::class);
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function price(): float
    {
        return $this->price / 100;
    }

    public function available(): bool
    {
        return $this->available;
    }

    public function sellerId(): int
    {
        return $this->seller_id;
    }
}
