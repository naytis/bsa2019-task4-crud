<?php

declare(strict_types=1);

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class OrderItem
 * @package App\Entity
 * @property int $id
 * @property int $quantity
 * @property int price
 * @property int $discount
 * @property int $order_id
 * @property int $product_id
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 */
final class OrderItem extends Model
{
    protected $table = 'order_items';

    protected $fillable = [
        'quantity',
        'price',
        'discount',
        'order_id',
        'product_id'
    ];

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function id(): int
    {
        return $this->id;
    }

    public function quantity(): int
    {
        return $this->quantity;
    }

    public function price(): float
    {
        return $this->price / 100;
    }

    public function discount(): int
    {
        return $this->discount;
    }

    public function orderId(): int
    {
        return $this->order_id;
    }

    public function productId(): int
    {
        return $this->product_id;
    }

    public function sum(): float
    {
        return round($this->price() * (1 - $this->discount() / 100) * $this->quantity(), 2);
    }
}
