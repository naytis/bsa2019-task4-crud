<?php

declare(strict_types=1);

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Order
 * @package App\Entity
 * @property int $id
 * @property int $buyer_id
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 * @property OrderItem[] $orderItems
 */
final class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'buyer_id'
    ];

    public function buyer(): BelongsTo
    {
        return $this->belongsTo(Buyer::class);
    }

    public function orderItems(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    public function id(): int
    {
        return $this->id;
    }

    public function date(): Carbon
    {
        return $this->created_at;
    }

    public function buyerId(): int
    {
        return $this->buyer_id;
    }

    public function sum(): float
    {
        $orderSum = 0;

        foreach ($this->orderItems as $orderItem) {
            $orderSum += $orderItem->sum();
        }

        return round($orderSum, 2);
    }
}
