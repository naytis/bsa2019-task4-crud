<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Action\Order\AddOrderAction;
use App\Action\Order\AddOrderRequest;
use App\Action\Order\DeleteOrderAction;
use App\Action\Order\GetAllOrdersAction;
use App\Action\Order\GetOrderByIdAction;
use App\Action\Order\UpdateOrderAction;
use App\Action\Order\UpdateOrderRequest;
use App\Http\Requests\AddOrderHttpRequest;
use App\Http\Requests\UpdateOrderHttpRequest;
use App\Http\Resources\OrderResource;
use App\Http\Response\ApiResponse;

final class OrderController extends ApiController
{
    private $getOrderCollectionAction;
    private $addOrderAction;
    private $getOrderByIdAction;
    private $updateOrderAction;
    private $deleteOrderAction;

    public function __construct(
        GetAllOrdersAction $getOrderCollectionAction,
        AddOrderAction $addOrderAction,
        GetOrderByIdAction $getOrderByIdAction,
        UpdateOrderAction $updateOrderAction,
        DeleteOrderAction $deleteOrderAction
    ) {
        $this->getOrderCollectionAction = $getOrderCollectionAction;
        $this->addOrderAction = $addOrderAction;
        $this->getOrderByIdAction = $getOrderByIdAction;
        $this->updateOrderAction = $updateOrderAction;
        $this->deleteOrderAction = $deleteOrderAction;
    }

    public function index(): ApiResponse
    {
        $orders = $this->getOrderCollectionAction->execute()->orders();
        return $this->createSuccessResponse(OrderResource::collection($orders));
    }

    public function store(AddOrderHttpRequest $request): ApiResponse
    {
        $order = $this->addOrderAction->execute(
            new AddOrderRequest(
                (int)$request->input('buyerId'),
                $request->input('orderItems')
            )
        )->order();

        return $this->created(new OrderResource($order));
    }

    public function show(int $id): ApiResponse
    {
        $order = $this->getOrderByIdAction->execute($id)->order();
        return $this->createSuccessResponse(new OrderResource($order));
    }

    public function update(UpdateOrderHttpRequest $request, int $id): ApiResponse
    {
        $order = $this->updateOrderAction->execute(
            new UpdateOrderRequest($id, $request->input('orderItems'))
        )->order();

        return $this->createSuccessResponse(new OrderResource($order));
    }

    public function destroy(int $id): ApiResponse
    {
        $this->deleteOrderAction->execute($id);
        return $this->createDeletedResponse();
    }
}
