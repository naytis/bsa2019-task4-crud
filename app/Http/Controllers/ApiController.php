<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Response\ApiResponse;
use Illuminate\Http\Resources\Json\JsonResource;

abstract class ApiController extends Controller
{
    final protected function createSuccessResponse(JsonResource $data): ApiResponse
    {
        return ApiResponse::success($data);
    }

    final protected function createDeletedResponse(): ApiResponse
    {
        return ApiResponse::deleted();
    }

    final protected function created(JsonResource $data): ApiResponse
    {
        return ApiResponse::created($data);
    }
}
