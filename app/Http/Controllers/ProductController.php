<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Action\Product\AddProductAction;
use App\Action\Product\AddProductRequest;
use App\Action\Product\DeleteProductAction;
use App\Action\Product\DeleteProductsBySellerIdAction;
use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetProductByIdAction;
use App\Action\Product\UpdateProductAction;
use App\Action\Product\UpdateProductRequest;
use App\Http\Requests\AddProductHttpRequest;
use App\Http\Resources\ProductResource;
use App\Http\Response\ApiResponse;
use Illuminate\Http\Request;

final class ProductController extends ApiController
{
    private $getAllProductsAction;
    private $addProductAction;
    private $getProductByIdAction;
    private $updateProductAction;
    private $deleteProductAction;
    private $deleteProductsBySellerIdAction;

    public function __construct(
        GetAllProductsAction $getAllProductsAction,
        AddProductAction $addProductAction,
        GetProductByIdAction $getProductByIdAction,
        UpdateProductAction $updateProductAction,
        DeleteProductAction $deleteProductAction,
        DeleteProductsBySellerIdAction $deleteProductsBySellerIdAction
    ) {
        $this->getAllProductsAction = $getAllProductsAction;
        $this->addProductAction = $addProductAction;
        $this->getProductByIdAction = $getProductByIdAction;
        $this->updateProductAction = $updateProductAction;
        $this->deleteProductAction = $deleteProductAction;
        $this->deleteProductsBySellerIdAction = $deleteProductsBySellerIdAction;
    }

    public function index(): ApiResponse
    {
        $products = $this->getAllProductsAction->execute()->products();
        return $this->createSuccessResponse(ProductResource::collection($products));
    }

    public function store(AddProductHttpRequest $request): ApiResponse
    {
        $product = $this->addProductAction->execute(
            new AddProductRequest(
                $request->input('productName'),
                (float)$request->input('productPrice'),
                (bool)$request->input('productAvailable'),
                (int)$request->input('sellerId')
            )
        )->product();

        return $this->created(new ProductResource($product));
    }

    public function show(int $id): ApiResponse
    {
        $product = $this->getProductByIdAction->execute($id)->product();
        return $this->createSuccessResponse(new ProductResource($product));
    }

    public function update(Request $request, int $id)
    {
        $product = $this->updateProductAction->execute(
            new UpdateProductRequest(
                $id,
                $request->input('productName'),
                (float)$request->input('productPrice'),
                (bool)$request->input('productAvailable'),
                (int)$request->input('sellerId')
            )
        )->product();

        return $this->createSuccessResponse(new ProductResource($product));
    }

    public function destroy(int $id)
    {
        $this->deleteProductAction->execute($id);
        return $this->createDeletedResponse();
    }

    public function deleteProductsBySellerId(int $sellerId)
    {
        $this->deleteProductsBySellerIdAction->execute($sellerId);
        return $this->createDeletedResponse();
    }
}
