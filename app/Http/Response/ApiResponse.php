<?php

declare(strict_types=1);

namespace App\Http\Response;

use App\Exceptions\ErrorCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

final class ApiResponse extends JsonResponse
{
    private const CLIENT_ERROR_STATUS = 400;
    private const NO_CONTENT_STATUS = 204;
    private const RESOURCE_NOT_FOUND_STATUS = 404;
    private const RESOURCE_CREATED_STATUS = 201;

    public static function error(string $code, string $message): self
    {
        static::assertErrorDataIsValid($code, $message);

        return new static([
            'errors' => [
                [
                    'code' => $code,
                    'message' => $message
                ]
            ]
        ], self::CLIENT_ERROR_STATUS);
    }

    public static function success(JsonResource $data): self
    {
        return new static(['data' => $data]);
    }

    public static function deleted(): self
    {
        return new static(null, self::NO_CONTENT_STATUS);
    }

    public static function notFound(string $message): self
    {
        return static::error(ErrorCode::NOT_FOUND, $message)->setStatusCode(self::RESOURCE_NOT_FOUND_STATUS);
    }

    private static function assertErrorDataIsValid(string $code, string $message): void
    {
        if (empty($code) || empty($message)) {
            throw new \InvalidArgumentException('Error values cannot be empty.');
        }
    }

    public static function created(JsonResource $data): self
    {
        return new static(['data' => $data], self::RESOURCE_CREATED_STATUS);
    }
}