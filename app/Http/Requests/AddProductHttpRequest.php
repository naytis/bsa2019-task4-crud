<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class AddProductHttpRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'productName' => 'required|string',
            'productPrice' => 'required|numeric|min:0',
            'productAvailable' => 'required|boolean',
            'sellerId' => 'required|integer|min:1'
        ];
    }
}
