<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class UpdateOrderHttpRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'orderItems' => 'required|array',
            'orderItems.*' => 'array',
            'orderItems.*.productId' => 'distinct|required|integer|min:1',
            'orderItems.*.productQty' => 'required|integer|min:1',
            'orderItems.*.productDiscount' => 'required|integer|between:0,100',
        ];
    }
}
