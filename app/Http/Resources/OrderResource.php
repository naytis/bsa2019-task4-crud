<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

final class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'orderId' => $this->id(),
            'orderDate' => $this->date(),
            'orderSum' => $this->sum(),
            'orderItems' => OrderItemResource::collection($this->orderItems()->get()),
            'buyer' => new BuyerResource($this->buyer()->first())
        ];
    }
}
