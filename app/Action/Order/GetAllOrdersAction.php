<?php

declare(strict_types=1);

namespace App\Action\Order;

use App\Entity\Order;

final class GetAllOrdersAction
{
    public function execute(): GetAllOrdersResponse
    {
        $orders = Order::all();

        return new GetAllOrdersResponse($orders);
    }
}