<?php

declare(strict_types=1);

namespace App\Action\Order;

final class UpdateOrderRequest
{
    private $id;
    private $orderItems;

    public function __construct(int $id, array $orderItems)
    {
        $this->id = $id;
        $this->orderItems = $orderItems;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function orderItems(): array
    {
        return $this->orderItems;
    }
}