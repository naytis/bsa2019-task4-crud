<?php

declare(strict_types=1);

namespace App\Action\Order;

final class AddOrderRequest
{
    private $buyerId;
    private $orderItems;

    public function __construct(int $buyerId, array $orderItems)
    {
        $this->buyerId = $buyerId;
        $this->orderItems = $orderItems;
    }

    public function buyerId(): int
    {
        return $this->buyerId;
    }

    public function orderItems(): array
    {
        return $this->orderItems;
    }
}