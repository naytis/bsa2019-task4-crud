<?php

declare(strict_types=1);

namespace App\Action\Order;

use App\Entity\Order;

final class AddOrderResponse
{
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function order(): Order
    {
        return $this->order;
    }
}