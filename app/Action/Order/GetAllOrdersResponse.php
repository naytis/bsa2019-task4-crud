<?php

declare(strict_types=1);

namespace App\Action\Order;

use Illuminate\Database\Eloquent\Collection;

final class GetAllOrdersResponse
{
    private $orders;

    public function __construct(Collection $orders)
    {
        $this->orders = $orders;
    }

    public function orders(): Collection
    {
        return $this->orders;
    }
}