<?php

declare(strict_types=1);

namespace App\Action\Order;

use App\Entity\Order;
use App\Exceptions\OrderNotFoundException;

final class DeleteOrderAction
{
    public function execute(int $id): void
    {
        $result = Order::destroy($id);
        if (!$result) {
            throw new OrderNotFoundException();
        }
    }
}