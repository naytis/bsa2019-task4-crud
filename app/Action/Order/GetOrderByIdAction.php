<?php

declare(strict_types=1);

namespace App\Action\Order;

use App\Entity\Order;
use App\Exceptions\OrderNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class GetOrderByIdAction
{
    public function execute(int $id): GetOrderByIdResponse
    {
        try {
            $order = Order::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new OrderNotFoundException();
        }

        return new GetOrderByIdResponse($order);
    }
}