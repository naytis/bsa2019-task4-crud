<?php

declare(strict_types=1);

namespace App\Action\Order;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Exceptions\OrderNotFoundException;
use App\Exceptions\ProductNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class UpdateOrderAction
{
    public function execute(UpdateOrderRequest $request): UpdateOrderResponse
    {
        try {
            $order = Order::findOrFail($request->id());
        } catch (ModelNotFoundException $e) {
            throw new OrderNotFoundException();
        }

        foreach ($request->orderItems() as $newOrderItem) {
            try {
                $product = Product::findOrFail($newOrderItem['productId']);
            } catch (ModelNotFoundException $e) {
                throw new ProductNotFoundException();
            }

            OrderItem::updateOrCreate([
                'price' => $product->price(),
                'order_id' => $order->id(),
                'product_id' => $product->id()
            ], [
                'quantity' => $newOrderItem['productQty'],
                'discount' => $newOrderItem['productDiscount']
            ]);
        }

        return new UpdateOrderResponse($order);
    }
}