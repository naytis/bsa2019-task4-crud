<?php

declare(strict_types=1);

namespace App\Action\Order;

use App\Entity\Buyer;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Exceptions\BuyerNotFoundException;
use App\Exceptions\ProductNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

final class AddOrderAction
{
    public function execute(AddOrderRequest $request): AddOrderResponse
    {
        $order = new Order();

        DB::transaction(function () use ($order, $request) {
            try {
                $buyer = Buyer::findOrFail($request->buyerId());
            } catch (ModelNotFoundException $e) {
                throw new BuyerNotFoundException();
            }

            $order->buyer_id = $buyer->id;
            $order->save();

            foreach ($request->orderItems() as $orderItem) {
                try {
                    $product = Product::findOrFail($orderItem['productId']);
                } catch (ModelNotFoundException $e) {
                    throw new ProductNotFoundException();
                }

                OrderItem::create([
                    'quantity' => $orderItem['productQty'],
                    'price' => $product->price(),
                    'discount' => $orderItem['productDiscount'],
                    'order_id' => $order->id(),
                    'product_id' => $product->id()
                ]);
            }
        });

        return new AddOrderResponse($order);
    }
}