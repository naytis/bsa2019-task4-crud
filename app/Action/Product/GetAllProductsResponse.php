<?php

declare(strict_types=1);

namespace App\Action\Product;

use Illuminate\Database\Eloquent\Collection;

final class GetAllProductsResponse
{
    private $products;

    public function __construct(Collection $products)
    {
        $this->products = $products;
    }

    public function products(): Collection
    {
        return $this->products;
    }
}