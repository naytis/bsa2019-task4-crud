<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Exceptions\ProductNotFoundException;

final class DeleteProductAction
{
    public function execute(int $id): void
    {
        $result = Product::destroy($id);
        if (!$result) {
            throw new ProductNotFoundException();
        }
    }
}