<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;

final class GetAllProductsAction
{
    public function execute(): GetAllProductsResponse
    {
        $products = Product::all();

        return new GetAllProductsResponse($products);
    }
}