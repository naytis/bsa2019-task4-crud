<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;

final class GetProductByIdResponse
{
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function product(): Product
    {
        return $this->product;
    }
}