<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Exceptions\ProductNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class GetProductByIdAction
{
    public function execute(int $id): GetProductByIdResponse
    {
        try {
            $product = Product::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new ProductNotFoundException();
        }

        return new GetProductByIdResponse($product);
    }
}