<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Exceptions\ProductNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class UpdateProductAction
{
    public function execute(UpdateProductRequest $request): UpdateProductResponse
    {
        try {
            $product = Product::findOrFail($request->id());
        } catch (ModelNotFoundException $e) {
            throw new ProductNotFoundException();
        }

        $product->name = $request->name();
        $product->price = $request->price() * 100;
        $product->available = $request->available();
        $product->seller_id = $request->selleId();
        $product->save();

        return new UpdateProductResponse($product);
    }
}