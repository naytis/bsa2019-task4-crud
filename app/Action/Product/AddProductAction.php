<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;

final class AddProductAction
{
    public function execute(AddProductRequest $request): AddProductResponse
    {
        $product = Product::create([
            'name' => $request->name(),
            'price' => $request->price() * 100,
            'available' => $request->available(),
            'seller_id' => $request->selleId()
        ]);

        return new AddProductResponse($product);
    }
}