<?php

declare(strict_types=1);

namespace App\Action\Product;

final class AddProductRequest
{
    private $name;
    private $price;
    private $available;
    private $selleId;

    public function __construct(
        string $name,
        float $price,
        bool $available,
        int $sellerId
    ) {
        $this->name = $name;
        $this->price = $price;
        $this->available = $available;
        $this->selleId = $sellerId;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function price(): float
    {
        return $this->price;
    }

    public function available(): bool
    {
        return $this->available;
    }

    public function selleId(): int
    {
        return $this->selleId;
    }
}