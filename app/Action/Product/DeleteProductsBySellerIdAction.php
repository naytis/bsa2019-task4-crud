<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Entity\Seller;
use App\Exceptions\SellerNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class DeleteProductsBySellerIdAction
{
    public function execute(int $sellerId): void
    {
        try {
            Seller::findOrFail($sellerId);
        } catch (ModelNotFoundException $e) {
            throw new SellerNotFoundException();
        }

        Product::where('seller_id', $sellerId)->delete();
    }
}